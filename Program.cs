﻿using System;
using System.Diagnostics;
using System.Linq;

namespace SBMatt
{
    class Program
    {
        static void Main()
        {
            int t = 100000;

            var sw = Stopwatch.StartNew();
            for (int i = 1; i <= t; i++)
            {
                Solve(i);
            }
            sw.Stop();
            Console.WriteLine($"{sw.ElapsedMilliseconds} ms");
            Console.ReadLine();
        }

        static string Solve(int n)
        {
            return new[] { 1, 2, 4, 7 }.Contains(n) ? "-1"
                : n % 3 == 0 ? new string('5', n)
                : (n - 5) % 3 == 0 ? new string('5', n - 5) + new string('3', 5)
                : new string('5', n - 10) + new string('3', 10);
        }
    }
}
